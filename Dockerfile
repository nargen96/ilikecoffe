FROM nginx:latest
COPY dist/Backoffice/ /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
