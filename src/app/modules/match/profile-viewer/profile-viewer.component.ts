import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/model/user';
import { AuthService } from 'src/app/service/auth.service';
import { UserService } from 'src/app/service/user.service';
import { AlertService } from '../../shared/services/alert.service';

@Component({
  selector: 'app-profile-viewer',
  templateUrl: './profile-viewer.component.html',
  styleUrls: ['./profile-viewer.component.css']
})
export class ProfileViewerComponent implements OnInit {

  public currentUser : User ;

  constructor(
    public authService : AuthService,
    private route : ActivatedRoute,
    private alertService : AlertService,
    public userService : UserService
  ) { 

    this.route.params.subscribe(routeParams => {
      if(routeParams.uid)
       {
         this.changeUser(routeParams.uid);
       }
    })

  }

  

  changeUser(uid:string){
    this.userService.findByUid(uid).subscribe(user=>{
      if(user){
        this.currentUser = new User();
        this.currentUser.aboutme = user.aboutme;
        this.currentUser.birth = user.birth;
        this.currentUser.displayName = user.displayName;
        this.currentUser.photoURL = user.photoURL;
        this.currentUser.sex = user.sex;
        
      }else{
        this.alertService.warn("User not found !");
      }
    });
  }

  ngOnInit(): void {
    

  }

}
