import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { User } from 'src/app/model/user';
import { AuthService } from 'src/app/service/auth.service';
import { AlertService } from '../../shared/services/alert.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css']
})
export class MyProfileComponent implements OnInit {

  user : User;
  cachedUser : User;
  hide = true;
  register = false;
  editing = false;
  isReady : Boolean = false;

    //Profile edition validation

    groupValdation: FormGroup = new FormGroup({
      sex :  new FormControl('', [
        Validators.required]
        ),
    
      interest : new FormControl('', [
        Validators.required]
        ),
    
      name : new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(50)]
        ),
    
      aboutMe :  new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(1000)]
        ),
    
      birth :  new FormControl('', [
        Validators.required]
        ),
    });
  

  constructor(
    public authService : AuthService,
    private alertService : AlertService
  ) { 
    
    this.authService.user.subscribe(user=>{
      this.fillFormGroup(user);
    });
  }
 

  fillFormGroup(user:User){
    if(user){

      this.cachedUser = new User();
      this.cachedUser.uid = user.uid;
      this.cachedUser.email = user.email;
      this.cachedUser.emailVerified = user.emailVerified;
      this.cachedUser.isReady = user.isReady;
      this.cachedUser.photoURL = user.photoURL;
      this.cachedUser.birth = user.birth;
      this.cachedUser.displayName = user.displayName;
      this.cachedUser.aboutme = user.aboutme;

      this.groupValdation.disable();

      this.groupValdation.get('name').setValue(user.displayName);
      this.groupValdation.get('birth').setValue(user.birth);
      this.groupValdation.get('aboutMe').setValue(user.aboutme);
      this.groupValdation.get('sex').setValue(user.sex);
      this.groupValdation.get('interest').setValue(user.interest);
      this.groupValdation.enable();
      this.isReady = user.isReady;
    }
  }

  ngOnInit(): void {
    //handle cached user
    this.fillFormGroup(this.authService.cachedUser);
  }

  getErrorMessage() {
    if (this.groupValdation.get('name').hasError('required') || this.groupValdation.get('aboutMe').hasError('required') ) {
      return 'You must enter a value';
    }else  if(this.groupValdation.get('name').hasError('name')){
      return 'Not a valid name';
    }else{
      return 'Not a valid description'
    }

  }

  enableEdition():void{
    this.editing = true;
  }

  checkFiles():boolean{
    // Create a root reference
    var storageRef = firebase.default.storage().ref();

    // Create a reference to 'mountains.jpg'
    let pathImg = 'profiles/images/'+this.cachedUser.uid+".jpg";
    var mountainsRef = storageRef.child(pathImg);
   // var file = new File(); // use the Blob or File API
    var x = document.getElementById("fileUpload");
    if ('files' in x ) {
      let file = x['files'][0];
      if(file && this.groupValdation.valid){
        this.cachedUser.photoURL = pathImg;
      
      mountainsRef.put( file as File ).then((snapshot)=> {
        this.alertService.ok("Profile image uploaded, refresh page !");
      });
      return true;

    }else{
      return false;
    }
    }


  }
  hasModifications(){
    let hasNewPic = this.checkFiles();
    return this.groupValdation.touched || hasNewPic;
  }

  saveProfile():void{

    this.groupValdation.markAsTouched();
    this.cachedUser.isReady = this.cachedUser.isReady;
    this.cachedUser.aboutme = this.groupValdation.get('aboutMe').value;
    this.cachedUser.birth = this.groupValdation.get('birth').value;
    this.cachedUser.displayName = this.groupValdation.get('name').value;
    this.cachedUser.interest= this.groupValdation.get('interest').value;
    this.cachedUser.sex= this.groupValdation.get('sex').value;
    
    if(this.hasModifications()){
      if(this.groupValdation.valid){

        this.editing = false;

        if(this.cachedUser.isReady){
          //update
          this.authService.updateUser(this.cachedUser);

        }else{
          //create
          this.authService.saveUser(this.cachedUser);
        }
        //this.cachedUser.photoURL= this.groupValdation.get('photoURL').value
      }else{
        this.alertService.info("Please, check your data");
      }
    }

  }

}
