import { Route } from '@angular/compiler/src/core';
import { toBase64String } from '@angular/compiler/src/output/source_map';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { Chat } from 'src/app/model/chat';
import { ChatService } from 'src/app/service/chat.service';

export interface PeriodicElement {
  name: string;

}

const ELEMENT_DATA: Chat[] = [
];

/**
 * @title Basic use of `<table mat-table>`
 */
@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit,AfterViewInit  {

  displayedColumns: string[] = ['position', 'name','other'];
  dataSource : Array<Chat> = [];

  constructor(
    private chatService : ChatService,
    private route : Router
  ) {
    this.chatService.chats.subscribe(chats=>{
      this.dataSource = chats;
    })
   }

   openChat(chatId:string,name:string,approved:boolean,userId:String){
     this.route.navigate(['chat','talk'],{ state:{
       chatId: chatId, 
       name: name,
       approved:approved,
       userId:userId
      }});
   }

  ngAfterViewInit(): void {
   // 
   setTimeout(()=>{
    this.chatService.clearNotifications();

   },500);

  }

  ngOnInit(): void {
  }

}
