import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { User } from 'src/app/model/user';
import * as firebase from 'firebase';
import { AlertService } from '../../shared/services/alert.service';
import { ChatService } from 'src/app/service/chat.service';
import { AuthService } from 'src/app/service/auth.service';

const VAR_SKIPED_USERS = 'skiped';
let SKIPED_USERS;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  @Input('user')
  public usr : User; 
  
  @Input('myProfile')
  myProfile : Boolean=false;

  @Input('viewMode')
  viewMode : Boolean=false;


  @Input('editing')
  editing : Boolean = false;

  @Output('edit')
  edit : EventEmitter<Boolean> = new EventEmitter();

  @Output('next')
  next : EventEmitter<Boolean> = new EventEmitter();

  
  @Input('overflow')
  moreDetails : Boolean = true;

  profileImg : any = "/assets/images/loading-img.gif";

  sent : Boolean = false;
  
  constructor(
    private alertService : AlertService,
    private chatService : ChatService,
    private auth : AuthService
  ) { 
    let skipedUsers = localStorage.getItem(VAR_SKIPED_USERS);
    if(skipedUsers){
      SKIPED_USERS = JSON.parse(skipedUsers);
    }else{
      SKIPED_USERS = new Array();
    }

  }

  

  ngOnChanges(changes: SimpleChanges): void {
    this.validateImgs();

  }

  ngOnInit(): void {
    this.validateImgs();

  }

  copyLink():void{
    this.alertService.link(this.usr.uid);
  }
  validateImgs():void{
    if(this.usr){
      if(this.usr.photoURL){
        var storage = firebase.default.storage();
        var pathReference = storage.ref(this.usr.photoURL);
    
        pathReference.getDownloadURL().then((url)=> {
          this.profileImg = url;
  
    
        }).catch(function(error) {
          // Handle any errors
        });  
      }else{
        this.profileImg = "/assets/images/default-profile.jpg";
      }
    }
  }

  createChat(userIdToChat : String):void{
    if(this.auth.cachedUser){
      let varName ='C-'+userIdToChat;
      if(!sessionStorage.getItem(varName)){
        this.chatService.createChat().subscribe(chatId=>{
          this.chatService.addChatToUser(chatId,userIdToChat,false);
        });
        sessionStorage.setItem(varName,'true');
        this.sent = true;
      }else{
        this.sent = true;
      }
    }else{
      this.alertService.info("you must be logged in!")

    }


  }


  chatSend(){
    this.alertService.info("The message has already been sent and must be approved by the user")

  }

  nextUsr(uid : String ):void{
    SKIPED_USERS.push(uid);
    localStorage.setItem(VAR_SKIPED_USERS,JSON.stringify(SKIPED_USERS));
    this.next.emit(true);
  }

  enableEdition(){
    this.edit.emit(true);
  }

  more(){
    this.moreDetails = true;
  }
  close(){
    this.moreDetails = false;
  }

}
