import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { until } from 'protractor';
import { Mensaje } from 'src/app/model/mensaje';
import { AuthService } from 'src/app/service/auth.service';
import { ChatService } from 'src/app/service/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  private chatId: string;
  public userName: string;
  public nowDate : Date;
  public mensajes : Array<Mensaje> = new Array();
  public text : String="";
  public approved : boolean = true;
  public loaded : boolean = false;
  public userId : string ="";
  constructor(
    private chatService : ChatService,
    private router: Router,
    private auth : AuthService,
    private changeDetector : ChangeDetectorRef

  ) { 

    let data = this.router.getCurrentNavigation().extras.state;
  if(data){
      this.chatId = data.chatId;
      this.userName = data.name
      this.nowDate = new Date();
      this.approved = data.approved;
      this.userId = data.userId;
      this.chatService.getMessages(this.chatId).subscribe(msg=>{
        if(msg){
          this.mensajes.push(msg);
          this.loaded = true;

        }
      })
    }
  }

  isNew():boolean{
    return this.mensajes.length==0 && this.loaded;
  }

  isMyMsg(mUid:String):boolean{
    return this.auth.cachedUser.uid == mUid;
  }

  enviarMsg( ){
    if(!this.isNew()){
      this.sendMsg();
    }

  }
  sendMsg(){
    let msg = new Mensaje();
    msg.msg = this.text;
    msg.userId = this.auth.cachedUser.uid;
    msg.userName = this.auth.cachedUser.displayName;
    this.chatService.sendMsg(this.chatId,msg);
    this.text ="";
  }

  ngOnInit(): void {
  
    this.chatService.clearNotifications();
  }

  approveChat():void{
    this.chatService.approve(this.chatId,this.userId);
    this.text = "Accepted !";
    this.sendMsg();
  }

  disApproveChat():void{
    this.router.navigate(['']);
    //this.chatService.disApprove(this.chatId);

  }

}
