import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MatchviewerComponent } from './matchviewer/matchviewer.component';
import { ProfileComponent } from './profile/profile.component';
import { ChatComponent } from './chat/chat.component';
import { MessagesComponent } from './messages/messages.component';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import { MyProfileComponent } from './my-profile/my-profile.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import { MarkdownModule } from 'ngx-markdown';
import { ProfileViewerComponent } from './profile-viewer/profile-viewer.component';
import { MatCarouselModule } from '@ngmodule/material-carousel';
import {MatTableModule} from '@angular/material/table';

import { 
  AuthService as AuthGuard 
} from '../../service/auth.service';
import { ChatService } from 'src/app/service/chat.service';
import { UserService } from 'src/app/service/user.service';

const routes: Routes = [
  {
    path: 'profile',
    component: MyProfileComponent,
    canActivate: [AuthGuard] 

  },
  {
    path: '',
    pathMatch: 'full',
    component: MatchviewerComponent,
  },  
  {
    path: 'chat/talk',
    component: ChatComponent,
    canActivate: [AuthGuard] 

  },
  {
    path: 'chat',
    pathMatch: 'full',
    component: MessagesComponent,
    canActivate: [AuthGuard] 
  } ,
  {

    path: ':uid',
    component: ProfileViewerComponent,
  } 
];

@NgModule({
  declarations: [ ChatComponent, MessagesComponent, MyProfileComponent,ProfileComponent,MatchviewerComponent, ProfileViewerComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    RouterModule.forChild(routes),
    MatDatepickerModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    MatRadioModule,
    MarkdownModule.forRoot(), 
    MatCarouselModule,
    MatTableModule,
    FormsModule
    
  ],
  providers:[
    MatDatepickerModule,
  ]
})
export class MatchModule { }
