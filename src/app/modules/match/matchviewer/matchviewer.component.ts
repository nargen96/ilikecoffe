import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { User } from 'src/app/model/user';
import { AuthService } from 'src/app/service/auth.service';
import { UserService } from 'src/app/service/user.service';
import { AlertService } from '../../shared/services/alert.service';
import { ProfileComponent } from '../profile/profile.component';
import { MatCarouselComponent, MatCarouselSlide, MatCarouselSlideComponent } from '@ngmodule/material-carousel';

@Component({
  selector: 'app-matchviewer',
  templateUrl: './matchviewer.component.html',
  styleUrls: ['./matchviewer.component.css'],
  changeDetection: ChangeDetectionStrategy.Default,

})
export class MatchviewerComponent implements OnInit,AfterViewInit {

  public index : number = 0;

  @ViewChild("page")
  private page : MatCarouselComponent;

  public loggedUsrId :string;
  constructor(
    public authService : AuthService,
    private changeDetection : ChangeDetectorRef,    
    public userService : UserService
  ) { 
    this.authService.user.subscribe(user=>{
      if(user){
        let uid = user.uid;

        localStorage.setItem('uId',uid);
        this.loggedUsrId = user.uid;
      }

      if(user){
        if(user.interest=='male'){
          this.userService.getMaleUid();
        }else{
          this.userService.getFemaleUid();
        }
      }else{
        //TODO si no esta registrado mostrar mezcla
        this.userService.getAllUid();

      }


    });

  }
  ngAfterViewInit(): void {
  }

  nextAccount():void{
    this.page.next();
  }

  ngOnInit(): void {
    this.loggedUsrId = localStorage.getItem('uId');


  }

}
