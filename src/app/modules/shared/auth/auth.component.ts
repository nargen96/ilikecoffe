import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from 'src/app/service/auth.service';
import { LoginComponent } from '../login/login.component';
import { RegisterComponent } from '../register/register.component';
import { AlertService } from '../services/alert.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  constructor(
    public dialog: MatDialog,
    public authService : AuthService,
    public alertService : AlertService) {}

  openDialogEmailLogin(): void {
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '500px',
      position :{top:"50px"},
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
    });
  }

  openDialogRegister(): void {
    const dialogRef = this.dialog.open(RegisterComponent, {
      width: '250px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
    });
  }

  openDialogGoogleLogin() : void{
    this.authService.authWithGoogle().subscribe(resp=>{
      if(resp){
        this.dialog.closeAll();
       // this.alertService.ok("Welcome !");
      }else{
        this.alertService.error("Google authentication failed !");

      }
    })
  }


  ngOnInit(): void {
  }

}
