import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Observable, Subject } from 'rxjs';
import { AuthService } from 'src/app/service/auth.service';
import { SidenavService } from '../services/sidenav.service';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  isAuthenticated : Subject<Boolean>;

  @ViewChild('sidenav') public sidenav: MatSidenav;

  constructor(
    private sidenavService: SidenavService,
    private authService : AuthService
    ) {
      this.isAuthenticated = this.authService.isUserLogged();

  }
  
  ngAfterViewInit(): void {
    this.sidenavService.setSidenav(this.sidenav);
  }
  ngOnInit(): void {
  }

}
