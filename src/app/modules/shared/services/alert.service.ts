import { Injectable } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import Swal from 'sweetalert2';


@Injectable({
  providedIn: 'root'
})
export class AlertService {

  duration = 4500;

  constructor(private _snackBar: MatSnackBar) { }

  link(msg:string){
    Swal.fire({
      title: '<strong>Profile url</strong>',
      icon: 'info',
      html:
        'Send ' +
        '<a href="'+msg+'" target="_blank">this link</a> ' +
        ' to a friend !',
      showCloseButton: false,
      showCancelButton: false,
      focusConfirm: false
    })
  }

  info(msg : string){
     
    Swal.fire({
      position:'top',
      icon: 'info',
      backdrop:false,
      title:msg,
      showConfirmButton: false,
      timer: this.duration
    })

  }

  warn(msg : string){
    Swal.fire({
      position:'top',
      backdrop:false,
      icon: 'warning',
      title:msg,
      showConfirmButton: false,
      timer: this.duration
    })
  }

  error(msg : string){
    Swal.fire({
      position:'top',
      backdrop:false,
      icon: 'error',
      title:msg,
      showConfirmButton: false,
      timer: this.duration
    })
  }

  ok(msg: string){
    Swal.fire({
      position:'top',
      backdrop:false,
      icon: 'success',
      title:msg,
      showConfirmButton: false,
      timer: this.duration
    })
  }
}
