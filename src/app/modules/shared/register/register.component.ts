import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { AlertService } from '../services/alert.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  email  = new FormControl('', [Validators.required, Validators.email]);
  password : FormControl=  new FormControl('', [Validators.required,Validators.minLength(6)]);
  password2 = new FormControl('', [Validators.required]);

  hide = true;
  
  constructor(
    private authService : AuthService,
    private alertService : AlertService) { }

  ngOnInit(): void {
  }

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }

    return this.email.hasError('email') ? 'Not a valid email' : '';
  }

  createAccount() : void{
    if(this.email.valid && this.password.valid)
      if(this.password.value == this.password2.value){
        this.authService.createUser(      
          this.email.value,
          this.password.value
        ).subscribe(resp=>{
          if(resp){
            this.alertService.ok("Welcome !"); 
          }else{
            this.alertService.ok("User already exists !");
          }
        })
      }else{
        this.alertService.error("Password didnt match");

      }

  }

}
