import { ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatDrawer, MatDrawerContainer, MatSidenav } from '@angular/material/sidenav';
import { Subject } from 'rxjs';
import { AuthService } from 'src/app/service/auth.service';
import { ChatService } from 'src/app/service/chat.service';
import { SidenavService } from '../services/sidenav.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  @Input() title: string;
  isAuthenticated : Subject<Boolean>;
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;

  
  constructor(
    private sideNavService : SidenavService,
    public authService : AuthService,
    public chatService : ChatService,
    private changeDetector : ChangeDetectorRef){
    this.sideNavService = sideNavService;
    this.isAuthenticated = this.authService.isUserLogged();
    
 
  }

  ngOnInit(): void {
}

  toggleSideNav():void{
    this.sideNavService.toggle();
  }

  endSession():void{
    this.authService.signOut();
  }

}
