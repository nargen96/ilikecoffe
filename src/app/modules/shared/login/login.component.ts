import { Component, OnInit } from '@angular/core';
import { FormControl,  Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { AlertService } from '../services/alert.service';



@Component({
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.css']})
export class LoginComponent implements OnInit {

    email : FormControl  = new FormControl('', [Validators.required, Validators.email]);
    password : FormControl=  new FormControl('', [Validators.required,Validators.minLength(6)]);
    hide = true;
    register = false;

    constructor(
      private authService : AuthService,
      private alertService : AlertService) {

    }

    ngOnInit() {
       
    }

    getErrorMessage() {
      if (this.email.hasError('required')) {
        return 'You must enter a value';
      }
  
      return this.email.hasError('email') ? 'Not a valid email' : '';
    }

    doRegistry():void{
      this.register = true;
    }

    doLogin():void{
      this.register = false;
    }

    authenticate() : void{
      if(this.email.valid && this.password.valid)
      this.authService.loginUser(      
        this.email.value,
        this.password.value
      ).subscribe(resp=>{
        if(resp){
          this.alertService.ok("Welcome ! ")
        }else{
          this.alertService.warn("Bad credentials ! ")

        }
      })
    }
}
