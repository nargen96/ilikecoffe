export class Chat {
  userId : String;
  userName : String;
  userPicture:String;
  chatId:String;
  createAt : Date = new Date();
  msg? : String;
  approved : boolean = false;
}
