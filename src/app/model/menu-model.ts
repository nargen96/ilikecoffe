export interface MenuNode {
  icon?: string;
  name: string;
  children?: MenuNode[];
  route?:string;
}