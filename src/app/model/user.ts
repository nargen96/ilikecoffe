export class User {
  displayName: string='Anonymous';
  email: string='';
  emailVerified: boolean=false;
  photoURL: string='';
  aboutme:string="I'm dev 🤓";
  birth: Date = new Date();
  sex : string = 'male';
  interest : string='female';
  isReady : Boolean = false;
  uid : string ;
  chats : number[] = [];
}
