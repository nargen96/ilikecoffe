import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from 'src/app-routing/app-routing.module';
import * as firebase from 'firebase';
import { SharedModule } from './modules/shared/shared.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(){
      firebase.default.initializeApp({
      apiKey: "AIzaSyC5EnNUUbJBlUUQyaIyHZ7izSiEW5KI7jo",
      authDomain: "ilikecoffee-adbb9.firebaseapp.com",
      databaseURL: "https://ilikecoffee-adbb9.firebaseio.com",
      projectId: "ilikecoffee-adbb9",
      storageBucket: "ilikecoffee-adbb9.appspot.com",
      messagingSenderId: "634950672779",
      appId: "1:634950672779:web:a11a8476c963d6f8794f8e",
      measurementId: "G-1RQZ5XRVXH"
    })

  }
}
