import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { Observable } from 'rxjs';
import { MenuNode } from '../model/menu-model';

@Injectable({providedIn:'root'}) 
export class MenuService {

  menuItem : Observable<MenuNode>;

  constructor() { 
    this.menuItem = new Observable();
  }

  public getMenu():Observable<MenuNode[]>{
    return new Observable<MenuNode[]>();
  }
}
