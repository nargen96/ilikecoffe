import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import * as firebase from 'firebase';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { User } from '../model/user';
import { AlertService } from '../modules/shared/services/alert.service';
import { switchMap, take } from 'rxjs/operators';
import { ChatService } from './chat.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate{

  private  isUserAuthenticated : BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public user : BehaviorSubject<User> = new BehaviorSubject(null);
  public cachedUser : User;

  constructor(
    private alertService : AlertService
   ) { 

    firebase.default.auth().onAuthStateChanged((user) => {
      if (user) {
        this.isUserAuthenticated.next(true);
        this.findUserIfExists(user);
        return console.log('Habemus user 🎉',user);

      }
          this.isUserAuthenticated.next(false);
          this.user.next(null);
          return console.log('No habemus user 😭');

        });
  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):boolean{
    return this.cachedUser!=null;

  }

  findUserIfExists(firebaseUsr) : Subject<User>{
      firebase.default.database().ref('users/male/'+firebaseUsr.uid)
      .on('value',(usr )=>{
        let loggedUser = new User();
        if(usr.val()){
          loggedUser.uid = firebaseUsr.uid;
          loggedUser.sex = 'male';
          loggedUser.photoURL = usr.val().photoURL?usr.val().photoURL:'';
          loggedUser.isReady = true;
          loggedUser.interest = usr.val().interest;
          loggedUser.emailVerified = firebaseUsr.emailVerified;
          loggedUser.email = firebaseUsr.email;
          loggedUser.chats = usr.val().chats?usr.val().chats:[];
          let dispName = usr.val().displayName;
          if(dispName && dispName != ''){
            loggedUser.displayName = dispName;

          }
          loggedUser.birth =  usr.val().birth;
          loggedUser.aboutme = usr.val().aboutme;
          this.cachedUser = loggedUser;
          return this.user.next(loggedUser);
        }else{
          firebase.default.database().ref('users/female/'+firebaseUsr.uid)
          .on('value',(usr )=>{
            let loggedUser = new User();
            if(usr.val()){
              loggedUser.uid = firebaseUsr.uid;
              loggedUser.sex = 'female';
              loggedUser.photoURL = usr.val().photoURL;
              loggedUser.isReady = true;
              loggedUser.interest = usr.val().interest;
              loggedUser.emailVerified = firebaseUsr.emailVerified;
              loggedUser.email = firebaseUsr.email;
              loggedUser.displayName = usr.val().displayName;
              loggedUser.birth =  usr.val().birth;
              loggedUser.aboutme = usr.val().aboutme;
              loggedUser.chats = usr.val().chats?usr.val().chats:[];

              this.cachedUser = loggedUser;

              return this.user.next(loggedUser);
            }else{
              //temp user no registrado
              loggedUser.email = firebaseUsr.email;
              loggedUser.displayName = firebaseUsr.displayName;
              loggedUser.emailVerified = firebaseUsr.emailVerified;
              //para dejar foto de google lo saco porq aveces tira 403
              //loggedUser.photoURL = firebaseUsr.photoURL;
              loggedUser.uid = firebaseUsr.uid;
              this.cachedUser = loggedUser;

              return this.user.next(loggedUser);
    
            }
    
        });

        }

    });
    return null;
  }

  
  createUser(email, password):Subject<Boolean> {
    console.log('Creando el usuario con email ' + email);
    let loginObs = new Subject<Boolean>();
    firebase.default.auth().createUserWithEmailAndPassword(email, password)
    .then(function (user) {
      console.log('¡Creamos al usuario!');
      loginObs.next(true);
      loginObs.complete();
    })
    .catch(function (error) {
      console.error(error);
      loginObs.next(false);
      loginObs.complete();
    });
    return loginObs;
  }

  saveUser(user : User){
    //validar email
    if(user.emailVerified || true){
      firebase.default.database().ref('/users/'+user.sex+'/'+user.uid).set({
        displayName: user.displayName,
        photoURL: user.photoURL,
        aboutme: user.aboutme,
        birth: user.birth,
        interest : user.interest,
        isReady : true,
        email : user.email,
        emailVerified : user.emailVerified
      });
    }else{
      this.alertService.info("You must to validate your email !");
    }

  }
  updateUser(user : User){
    if(user.emailVerified || true){
      firebase.default.database().ref('/users/'+user.sex+'/'+user.uid).update({
        displayName: user.displayName,
        photoURL: user.photoURL,
        aboutme: user.aboutme,
        birth: user.birth,
        interest : user.interest,
        isReady : user.isReady,
        email : user.email,
        emailVerified : user.emailVerified
      });
    }else{
      this.alertService.info("You must to validate your email !");
    }

  }
  
  loginUser(email, password) : Subject<Boolean> {
    console.log('Loging user ' + email);
    let loginObs = new Subject<Boolean>();

    firebase.default.auth().signInWithEmailAndPassword(email, password)
    .then(function (user) {
      console.log('Credenciales correctas, ¡bienvenido!');
      loginObs.next(true);
      loginObs.complete();
    })
    .catch(function (error) {
      console.log(error);
      loginObs.next(false);
      loginObs.complete();
    });
    return loginObs;
  }
  
  signOut() : void {
    firebase.default.auth().signOut();
  }
  
  authWithGoogle() : Subject<Boolean> {
    let loginObs = new Subject<Boolean>();

    firebase.default.auth().signInWithPopup(    
      new firebase.default.auth.GoogleAuthProvider()
    ).then((result) => {
      console.log(result);
      console.log("google sign in");
      loginObs.next(true);
      loginObs.complete();
    })
    .catch(err => {
      console.log(err);
      loginObs.next(false);
      loginObs.complete();
    })

    return loginObs; 
  }

  public isUserLogged(): Subject<Boolean>{
    return this.isUserAuthenticated;
  }
}
