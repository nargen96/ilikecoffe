import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { Observable, Subject } from 'rxjs';

@Injectable({providedIn:'root'}) 
export class BlogService {


  markdownBlog : Subject<String>;

  constructor() { 
    this.markdownBlog = new Subject();
  }

  public getBlogMarkDownSubject():Observable<String>{
    return this.markdownBlog;
  }
  public getBlogMarkDown(blogId : string):Observable<String>{
    firebase.default.database().ref('post/'+blogId)
    .on('value',(snapshot)=>{

      try{
        this.markdownBlog.next(atob(snapshot.val()));
      }catch(error){
        this.markdownBlog.next( "Invalid value :( ");
      }

      //this.markdownBlog.complete();
  });
    return this.markdownBlog;
  
    
  }
}
