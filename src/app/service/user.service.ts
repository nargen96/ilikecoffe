import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { BehaviorSubject, Subject } from 'rxjs';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public userSubject = new Subject<User>();
  public users : BehaviorSubject<Array<User>> = new BehaviorSubject([]);
  private lastNumb : number = 0;
  constructor() { }

  findByUid(uid : string):Subject<User>{
      return this.findUserIfExists(uid);
   
  }
  getFemaleUid():void{
    this.getOtherUsers('female');      
  }

  getMaleUid():void{
    this.getOtherUsers('male');
  }

  getAllUid():void{
    this.getOtherUsers('female');   
  }
  
  getOtherUsers(sex:string){
    firebase.default.database().ref('users/'+sex)
    .on('value',(usrs)=>{
      let usuarios = new Array<User>();
      usrs.forEach(usr =>{
        let usuaurio = new User();
        if(usr.val()){
          usuaurio.uid = usr.key;
          usuaurio.sex = sex;
          if(usr.val().photoURL){
            usuaurio.photoURL = usr.val().photoURL;
          }
          usuaurio.interest = usr.val().interest;
          usuaurio.emailVerified = usr.val().emailVerified;
          usuaurio.birth =  usr.val().birth;
          usuaurio.aboutme = usr.val().aboutme;
         // loggedUser.email = '';
          let dispName = usr.val().displayName;
          if(dispName && dispName != ''){
            usuaurio.displayName = dispName;
          }

          usuarios.push(usuaurio);
  
      
        }
      }

      );
      this.users.next(usuarios);
      this.lastNumb+=5;
      })
  }



  private findUserIfExists(uid: string) : Subject<User>{

    firebase.default.database().ref('users/male/'+uid)
    .on('value',(usr )=>{
      let loggedUser = new User();
      if(usr.val()){
        loggedUser.uid = uid;
        loggedUser.sex = 'male';
        loggedUser.photoURL = usr.val().photoURL;
        loggedUser.isReady = true;
        loggedUser.interest = usr.val().interest;
        loggedUser.emailVerified = usr.val().emailVerified;
       // loggedUser.email = '';
        let dispName = usr.val().displayName;
        if(dispName && dispName != ''){
          loggedUser.displayName = dispName;
        }
        loggedUser.birth =  usr.val().birth;
        loggedUser.aboutme = usr.val().aboutme;
        
        this.userSubject.next(loggedUser);
      }else{
        firebase.default.database().ref('users/female/'+uid)
        .on('value',(usr )=>{
          let loggedUser = new User();
          if(usr.val()){
            loggedUser.uid = uid;
            loggedUser.sex = 'female';
            loggedUser.photoURL = usr.val().photoURL;
            loggedUser.isReady = true;
            loggedUser.interest = usr.val().interest;
            loggedUser.displayName = usr.val().displayName;
            loggedUser.birth =  usr.val().birth;
            loggedUser.aboutme = usr.val().aboutme;

            this.userSubject.next(loggedUser);
          }else{
            this.userSubject.next(null);
            
          }
  
      });

      }

  });
  return this.userSubject;
}
}
