import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { BehaviorSubject, Subject } from 'rxjs';
import { Chat } from '../model/chat';
import { Mensaje } from '../model/mensaje';
import { AlertService } from '../modules/shared/services/alert.service';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  public newMessages : BehaviorSubject<boolean> = new BehaviorSubject(false);
  public chats : BehaviorSubject<Array<Chat>> = new BehaviorSubject<Array<Chat>>([]);

  constructor(
    private authService : AuthService,
    private alertService : AlertService) {
    this.authService.user.subscribe(usr=>{
      if(usr){
        this.listenChats();
      }

    })
   }

  approve(chatId:string,otherUserId:string){
  this.addChatToUser(chatId,otherUserId,true);
  }

  disApprove(chatId:string){
  
    //firebase.default.database().ref("chats/"+chatId).remove();
    
  }
  listenChats():void{
    firebase.default.database().ref('/messages/'+this.authService.cachedUser.uid)
    .on('value',(usr )=>{
      let chats = usr.val();
      if(chats){
        this.newMessages.next(true);
        let chatList = new Array<Chat>();
        this.playSound();
        Object.keys(chats).forEach(chatKey=>{
          chatList.push(chats[chatKey]);
        });
        this.chats.next(chatList);
      }else{
        this.newMessages.next(false);
      }

    });

  }

  playSound(){
    //let audio = new Audio("/assets/sounds/notification.mp3");
    //audio.play();
  }

  clearNotifications(){
    this.newMessages.next(false);
  }

  createChat():Subject<String>{
    let chatID = new Subject<String>();

    firebase.default.database().ref('/chats').push({}).then(res=>{
      let chatId = res.key;
      chatID.next(chatId);
    }).catch(err=>{
      chatID.error("error");
    })

    return chatID; 
  }

  sendMsg(chatId : String,msg : Mensaje):void{

    firebase.default.database().ref('/chats/'+chatId).push(msg).then(res=>{
      //guardo chat en el otro usuario
    }).catch(err=>{
    })
  }

  addChatToUser(chatId : String,otherUserId : String,approval:boolean):void{
    let chat = new Chat();
    let displayName =  this.authService.cachedUser.displayName;
    chat.userName =  this.authService.cachedUser.displayName? this.authService.cachedUser.displayName:'Anonymous';
    chat.userPicture = this.authService.cachedUser.photoURL;
    chat.userId = this.authService.cachedUser.uid;
    chat.chatId = chatId;
    firebase.default.database().ref('/messages/'+otherUserId).push(chat).then(res=>{
      //guardo chat en el otro usuario
      if(approval){
        this.alertService.ok("You can chat now !")

      }else{
        this.alertService.ok("The message has been sent and must be approved by the user")

      }
    }).catch(err=>{
    })
  }

  getMessages(chatId : String):BehaviorSubject<Mensaje>{
  
    let mSub = new BehaviorSubject<Mensaje>(null);
    firebase.default.database().ref('/chats/'+chatId).limitToLast(10)
    .on('child_added',(msgs )=>{
      let m = msgs.val();
      this.playSound();

      if(m){
  
          mSub.next(m);
        


      }
      

    });
    return mSub;
  }

}
